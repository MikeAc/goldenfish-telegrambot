from Games import Games
from Games import Player
from Games import   Cards
from random import shuffle
import itertools

class FishGame(Games):
    """Docstring for FishGame. """
    CARDSOUT=1
    DELIVERY_SUCCESS=2
    ONCARDS=3
    GAMEOVER=4
    OK=5
    NEEDS_PLAYERS=6

    def __init__(self,id):
        """TODO: to be defined. """
        
        super().__init__(id)
        self.cards=[] 
        self.turnIt=0
        self.currPlayer=None
        self.status=FishGame.ONCARDS
        self.last_cardid=""
        
    @staticmethod
    def start(playerid,romid):
        
        game,player=Games.searchPlayer(playerid,romid) 
        if player is None or game is None:
            return None
        if len(game.playerlist)<=1:
            return FishGame.NEEDS_PLAYERS
        if game.started or game.isOpen is False:
            return FishGame.OK
        count=0

        for cardtype in Cards.cardtypes:  
            for i in  Cards.numbers:
                card=Cards(Cards.cardids[count],Cards.cardid_unique[count],cardtype,i)   

                game.cards.append(card)
                count+=1
        shuffle(game.cards)
        shuffle(game.playerlist)
        game.delivercards()
        game.iter()
        game.started=True
        game.isOpen=False
        game.currPlayer.status=FishPlayer.PLAYER_ASKS_CARD
        return game.currPlayer
    
    def iter(self):
        self.currPlayer=self.playerlist[self.turnIt%len(self.playerlist)]
        self.turnIt+=1
        
    def delivercards(self):
        try:
            for player in self.playerlist:
                for i in range(5):
                    card=self.cards.pop()
                    player.cards.append(card)
            return FishGame.DELIVERY_SUCCESS

        except:
            return FishGame.CARDSOUT

    def formatPlayerlist(self):
        text="Turno de: "+self.currPlayer.prefix+"\n"
        for player in self.playerlist:
                text+="{}  ".format(player.name)
                if player.cards:
                    text+="{} cartas: ".format(len(player.cards))
                for poker in player.pokers:
                    text+="{}, ".format(poker)
                text+="\n"

        if self.cards:
            text+="Cartas en el monto:{}\n".format(len(self.cards))

           
        return text
    
    def next(self):
        self.iter()
        players=len(self.playerlist)-1
        counter=0
        for player in self.playerlist:
            if player.status==FishPlayer.PLAYER_OUTOFCARDS:
                counter+=1
        if counter>=players:
            self.status=FishGame.GAMEOVER
            return FishGame.GAMEOVER


        
        while self.currPlayer.status==FishPlayer.PLAYER_OUTOFCARDS:
            self.iter()


        self.currPlayer.status=FishPlayer.PLAYER_ASKS_CARD
        return FishGame.OK

    def gameOver( self):
        winners=[]
        self.playerlist.sort(key=(lambda ar: 0 if not ar.pokers else len(ar.pokers)),reverse=True )
        
        player=self.playerlist[0]
        for bplayer in self.playerlist:
            if not bplayer.pokers:
                break

            if len(player.pokers)==len(bplayer.pokers):
                winners.append(bplayer)
            else:
                break

        if len(winners)>1:
            winners.sort(key=self.countTotalPokers,reverse=True )
            player=winners[0]
        return player


    def countTotalPokers(self,player):
        counter=0
        for poker in player.pokers:
            counter+=poker

        return counter
        
    def onremovePlayer(self,player):
        if not self.started :
            return
        shuffle(player.cards)
        self.cards.extend(player.cards)
        shuffle(self.cards)
        if self.turnIt != 0:
            self.turnIt-=1
        

            


        


   


class FishPlayer(Player):

    """Docstring for FishPlayer. """
    PLAYER_ASKS_CARD=1
    PLAYER_ASKS_PLAYER=2
    PLAYER_ASKS_NOTHING=3
    PAYER_ASKS_DRAW=4
    PLAYER_OUTOFCARDS=5
    def __init__(self,id,name,prefix):
        super().__init__(id,name,prefix) 
        self.cards=[]
        self.pokers=[]
        self.cardtoAsk={'card':None,'player':None}
        self.status=FishPlayer.PLAYER_ASKS_NOTHING

    def findCard(self,cardid):
        try:
            for card in self.cards:
                print("cardid='"+cardid+"',  card.id'"+card.id_unique+",")
                if card.id_unique==cardid:
                    return card
            return None
        except:
            return None


    def exchangeCards(self):
        bufflist=[]
        for card2 in self.cardtoAsk['player'].cards:
            if self.cardtoAsk['card'].number==card2.number:
                bufflist.append(card2)
        total=0 
        if not bufflist:
            self.status=FishPlayer.PAYER_ASKS_DRAW
        else:
            for card in bufflist:
                self.cardtoAsk['player'].cards.remove(card)
            if not self.cardtoAsk['player'].cards:
                self.cardtoAsk['player'].status=FishPlayer.PLAYER_OUTOFCARDS
            total=len(bufflist)
            self.cards.extend(bufflist)
            self.status=FishPlayer.PLAYER_ASKS_CARD
    
        return total
    def draw(self):
        if self.games[0].status==FishGame.ONCARDS:
            card=self.games[0].cards.pop()
            self.cards.append(card)
            if not self.games[0].cards:
                self.games[0].status=FishGame.CARDSOUT
            
            print(" Saved card number number {} ".format(self.cardtoAsk['card'].number))
            if  self.cardtoAsk['card'].number==card.number:
                self.status=FishPlayer.PLAYER_ASKS_CARD
            else:
                self.status=FishPlayer.PLAYER_ASKS_NOTHING
            return card
        return None

    def makePokers(self):
        if not self.cards:
            self.status=FishPlayer.PLAYER_OUTOFCARDS
            return 0
        for i in range(1,14):
            buffnumber=0
            for card in self.cards:
                if card.number==i:
                    buffnumber+=1
            
            if buffnumber >= 4:
                self.pokers.append(i)
                listbuff=[]
                for card2 in self.cards:
                    if card2.number == i:
                        listbuff.append(card2)

                for card2 in listbuff:
                    self.cards.remove(card2)
                if not self.cards:
                    self.status=FishPlayer.PLAYER_OUTOFCARDS
                return i
        return 0






                










            
                    



