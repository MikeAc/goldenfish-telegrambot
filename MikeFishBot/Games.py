
class Player (object):

    def __init__(self,id,name,prefix):
        self.id=id
        self.name=name
        self.prefix=prefix
        self.games=[]
        

 
class Games(object):
    gamelist=[]
    incremental_id=1
    NOGAMECREATED=2
    GAME_DELETED=7
    PLAYER_JOINED=1 
    PLAYER_ALREADY_JOINED=3
    PLAYER_REMOVED=4
    PLAYER_NOTFOUND=6
    """Docstring for Games. """
    def __init__(self,id):
        """TODO: to be defined. """
        self.romid=id
        self.id=0
        self.isOpen=True
        self.started=False
        self.playerlist=[]
        
    @staticmethod
    def createNewgame(game):
        for currgame in Games.gamelist:
            if game.romid == currgame.romid and currgame.isOpen : 
                return False
    
        Games.gamelist.append(game)
        game.id=Games.incremental_id
        Games.incremental_id+=1
        return True
    @staticmethod
    def searchPlayer(playerid,romid):
        try:
            for currgame in Games.gamelist:
                if currgame.romid == romid :
                    for listplayer in currgame.playerlist:
                        if listplayer.id == playerid:
                            return (currgame,listplayer)
                    return (currgame,None)
        except: 
            return None
        return None
    @staticmethod
    def searchPlayerbyId(playerid):
        try:
            for currgame in Games.gamelist:
                    for listplayer in currgame.playerlist:
                        if listplayer.id == playerid:
                            return (currgame,listplayer)
        except :
            return None
        return None
    
    @staticmethod
    def searchPlayerbyPref(prefix):
        try:
            for currgame in Games.gamelist:
                for listplayer in currgame.playerlist:
                    if listplayer.prefix == prefix:
                        return (currgame,listplayer)
        except :
            return None
        return None



    @staticmethod
    def killGame(id,romid):
        game,player=Games.searchPlayer(id,romid)
        if player is None:
            return Games.PLAYER_NOTFOUND
        elif game is None:
            return Games.NOGAMECREATED
        else: 
            for players in game.playerlist:
                players.games.remove(game)
                del players
            Games.gamelist.remove(game)
            del game
            return Games.GAME_DELETED


    @staticmethod
    def joinPlayer(player:Player,romid):
        try:
            game,buffplayer=Games.searchPlayer(player.id,romid)

            if game is None:
                return Games.NOGAMECREATED
            elif game.started:
                return Games.NOGAMECREATED
            elif buffplayer is None and  not player.games :
                player.games.append(game)
                game.playerlist.append(player)
                return Games.PLAYER_JOINED
            else:
                return Games.PLAYER_ALREADY_JOINED
        except Exception as e:
            print(e.args)
            return Games.NOGAMECREATED

    def onremovePlayer(self,player):
        pass

    @staticmethod
    def removePlayer(id,romid):
        try:
            game,player=Games.searchPlayer(id,romid)
            if game.started and len(game.playerlist)<=1:
                return Games.killGame(player.id,game.romid)
            player.games.remove(game)
            game.playerlist.remove(player)
            game.onremovePlayer(player)
            return Games.PLAYER_REMOVED
        except Exception as e:
            print(e.args)
            return Games.PLAYER_NOTFOUND
    @staticmethod
    def start(playerid,romid):
        pass

    def findPlayerbyPrefix(self,prefix):
        for  player in self.playerlist:
            if player.prefix==prefix:
                return player
        return None


class Cards(object):
    cardids=['CAACAgQAAxkBAAIBmF6X6kslvtSv6LcCySH3pJZUdeEmAAIQAQAC2aDGAAETWXiZubqsShgE',
            'CAACAgQAAxkBAAIBmV6X6lCBiLlcBe0yqkfvWn2hQsGKAAIRAQAC2aDGAAHsRx7yT-5_tRgE',
            'CAACAgQAAxkBAAIBml6X6lEKJDtUPpmKuV7ionSGqnVtAAISAQAC2aDGAAFDfJTwZjHlCRgE',
            'CAACAgQAAxkBAAIBm16X6lLAthP-HX7_T3jcwUDdQdVPAAITAQAC2aDGAAE3Ooar2drGERgE',
            'CAACAgQAAxkBAAIBnF6X6lLYemioU_2waeGHVKKr9BjFAAIUAQAC2aDGAAEemcamdno-PBgE',
            'CAACAgQAAxkBAAIBnV6X6lNt8IWvOM-soo0QH-_dgifRAAIVAQAC2aDGAAEN2bCvKFo8oRgE',
            'CAACAgQAAxkBAAIBnl6X6lVMruMWom0OVt5SrgABeQsCYQACFgEAAtmgxgABFxGXky0qRo4YBA',
            'CAACAgQAAxkBAAIBn16X6lbqr0BV-Hdh2yl1n0lztmrOAAIXAQAC2aDGAAGKge11xZTnhRgE',
            'CAACAgQAAxkBAAIBoF6X6labj6y9g2zAAUzE6xfzPSy8AAIYAQAC2aDGAAHCaYybb1Hs4hgE',
            'CAACAgQAAxkBAAIBoV6X6lfddJngp6e2G0rngmJ68iP-AAIZAQAC2aDGAAHxA0LIXTPU9hgE',
            'CAACAgQAAxkBAAIBol6X6lkZFkRtGlhnDwFY1DSMK0rtAAIaAQAC2aDGAAGZBKe_PTMqcBgE',
            'CAACAgQAAxkBAAIBo16X6lmyNPeatSMw16IMpQAB82OlTgACGwEAAtmgxgABQeBlq76OW4EYBA',
            'CAACAgQAAxkBAAIBpF6X6loTQsmnwMKO-cQRWKqS7TORAAIcAQAC2aDGAAHTUcM5kCYitBgE',
            'CAACAgQAAxkBAAIBpV6X6l69EUrYaarPRXoiVb3FfzSGAAIdAQAC2aDGAAH_sXvIonj3oBgE',
            'CAACAgQAAxkBAAIBpl6X6l9Oi5i25ug4sSfySuKbn2O2AAIeAQAC2aDGAAFcPCcJ-s2DQRgE',
            'CAACAgQAAxkBAAIBp16X6mDow8bjPx0a7dnjqjg8nNbbAAIfAQAC2aDGAAGMPJIsAAH11oQYBA',
            'CAACAgQAAxkBAAIBqF6X6mCDPtXVxfhOEi-KBbzQflEiAAIgAQAC2aDGAAH04fyV9A_nhBgE',
            'CAACAgQAAxkBAAIBqV6X6mFsO6RTdnLymP4dbq7z9KKFAAIhAQAC2aDGAAG4jFW1mIuXtRgE',
            'CAACAgQAAxkBAAIBql6X6mN_A7HiMtvoNlAsNVMKEJHgAAIiAQAC2aDGAAHDaxOOjQvOxBgE',
            'CAACAgQAAxkBAAIBq16X6mMPoUobGY8lcr5wqBvnmwYaAAIjAQAC2aDGAAFLktFU7iH-5xgE',
            'CAACAgQAAxkBAAIBrF6X6mXdvFYCTBMKzxdEqx1LAQlRAAIkAQAC2aDGAAFOFFMdiRp2SxgE',
            'CAACAgQAAxkBAAIBrV6X6mYRbaEji19z11pK4FQorm4KAAIlAQAC2aDGAAFFODEVi1mmQRgE',
            'CAACAgQAAxkBAAIBrl6X6mdFbPXttjeA0jbLj3pDL-gaAAImAQAC2aDGAAGoIcETOGePahgE',
            'CAACAgQAAxkBAAIBr16X6mgJnsGlC2y4r3CZimns1LNnAAInAQAC2aDGAAEs_Qyh78G5hBgE',
            'CAACAgQAAxkBAAIBsF6X6mlgY9TJWzGYSVxxacAKs6zkAAIoAQAC2aDGAAHRZdoQeiOw6xgE',
            'CAACAgQAAxkBAAIBsV6X6moqlWw2nHiK8G-GCIa_AAG0sgACKQEAAtmgxgABVqe44dY97qMYBA',
            'CAACAgQAAxkBAAIBsl6X6msUeFYs-pQ9eRgYWtLVYLOyAAIqAQAC2aDGAAHYpoUvx3gwLBgE',
            'CAACAgQAAxkBAAIBs16X6mwKuUMcjmqImo2uVAvVQ5BTAAIrAQAC2aDGAAE7ZGuqvLmxZBgE',
            'CAACAgQAAxkBAAIBtF6X6mxASbcxFjTGpNWCf_G31b8fAAIsAQAC2aDGAAFlLHq_iU1_hxgE',
            'CAACAgQAAxkBAAIBtV6X6m1k7yYjTfY_hI7FRERsXF1RAAItAQAC2aDGAAFKVGiaQTzvGRgE',
            'CAACAgQAAxkBAAIBtl6X6m7owyvAnHg_YXaFrc-kXXnMAAIuAQAC2aDGAAENNBaWpwz_RRgE',
            'CAACAgQAAxkBAAIBt16X6m7soa1AQlni5kltAREWu55-AAIvAQAC2aDGAAGLyR8nZatCchgE',
            'CAACAgQAAxkBAAIBuF6X6nAe7wW47xZMhe8wCBNe_1ZFAAIwAQAC2aDGAAG2wdbZCo4R4BgE',
            'CAACAgQAAxkBAAIBuV6X6nLCLs7M9hwM8b84Lmo_8OTUAAIxAQAC2aDGAAGIJPvJS1rF1BgE',
            'CAACAgQAAxkBAAIBul6X6nIo4Fd1vc8vOj__T-3S_z-uAAIyAQAC2aDGAAE89wR_bejuKBgE',
            'CAACAgQAAxkBAAIBu16X6nNSZyQLgTdtGZ9ghuSvNFBlAAIzAQAC2aDGAAGIejzaSiQFzRgE',
            'CAACAgQAAxkBAAIBvF6X6nTgZxUCt8oHXLt29Wfpqg8tAAI0AQAC2aDGAAFNS4K1IegM8BgE',
            'CAACAgQAAxkBAAIBvV6X6nVT_i8GQ8MubKc_Mmr1df3YAAI1AQAC2aDGAAFCfZ0nM_SwmhgE',
            'CAACAgQAAxkBAAIBvl6X6naz916CDTaiY_dIjM1WeFUMAAI2AQAC2aDGAAFgS3c08G6dzxgE',
            'CAACAgQAAxkBAAIBv16X6ngpOtzxJ8XnRaH3fEew2TUcAAI3AQAC2aDGAAHVdBBd0eLe-hgE',
            'CAACAgQAAxkBAAIBwF6X6nk_nxYSdpm3pRySXlKShkmjAAI4AQAC2aDGAAE7ft7vVS3c-hgE',
            'CAACAgQAAxkBAAIBwV6X6npXlfSt-V08oRYiR7talCyrAAI5AQAC2aDGAAGxVP6UKOJd9BgE',
            'CAACAgQAAxkBAAIBwl6X6nqM_r84mB4xaNWdWWOqIp9-AAI6AQAC2aDGAAFqQ_xuG6q40BgE',
            'CAACAgQAAxkBAAIBw16X6ntEfWw7NHlssQmVohKBm_6eAAI7AQAC2aDGAAHXkw0FxRPl-RgE',
            'CAACAgQAAxkBAAIBxF6X6nuE7Vlt0jC279eQ4EknRYgaAAI8AQAC2aDGAAFcNsDLiw9fJBgE',
            'CAACAgQAAxkBAAIBxV6X6nya5TZV-QOTZbKgUbeWufbLAAI9AQAC2aDGAAH4mDJHyXif4BgE',
            'CAACAgQAAxkBAAIBxl6X6n5nLBAs36ujYb0GQw4xMFTxAAI-AQAC2aDGAAGUJReZ10ayxRgE',
            'CAACAgQAAxkBAAIBx16X6n7h3uzVd2sHnIj3onYmg9rIAAI_AQAC2aDGAAHj5dEh2XgUahgE',
            'CAACAgQAAxkBAAIByF6X6n9vUkHjp4SS5XC9klXKQDKyAAJAAQAC2aDGAAHMHYhqRSqKgxgE',
            'CAACAgQAAxkBAAIByV6X6n-1NJC7PDCcVA9RgMH2QFUFAAJBAQAC2aDGAAHfF5JvwcgOhRgE',
            'CAACAgQAAxkBAAIByl6X6oCX_evvi4DmSpGRKee5kmoHAAJCAQAC2aDGAAEPrZ6BUT3g8BgE',
            'CAACAgQAAxkBAAIBy16X6oGb5jyYZ8ThgfJCj7iEJEiCAAJDAQAC2aDGAAG1HGMnJskZ7xgE']
            
    cardid_unique=['AgADEAEAAtmgxgAB',
            'AgADEQEAAtmgxgAB',
            'AgADEgEAAtmgxgAB',
            'AgADEwEAAtmgxgAB',
            'AgADFAEAAtmgxgAB',
            'AgADFQEAAtmgxgAB',
            'AgADFgEAAtmgxgAB',
            'AgADFwEAAtmgxgAB',
            'AgADGAEAAtmgxgAB',
            'AgADGQEAAtmgxgAB',
            'AgADGgEAAtmgxgAB',
            'AgADGwEAAtmgxgAB',
            'AgADHAEAAtmgxgAB',
            'AgADHQEAAtmgxgAB',
            'AgADHgEAAtmgxgAB',
            'AgADHwEAAtmgxgAB',
            'AgADIAEAAtmgxgAB',
            'AgADIQEAAtmgxgAB',
            'AgADIgEAAtmgxgAB',
            'AgADIwEAAtmgxgAB',
            'AgADJAEAAtmgxgAB',
            'AgADJQEAAtmgxgAB',
            'AgADJgEAAtmgxgAB',
            'AgADJwEAAtmgxgAB',
            'AgADKAEAAtmgxgAB',
            'AgADKQEAAtmgxgAB',
            'AgADKgEAAtmgxgAB',
            'AgADKwEAAtmgxgAB',
            'AgADLAEAAtmgxgAB',
            'AgADLQEAAtmgxgAB',
            'AgADLgEAAtmgxgAB',
            'AgADLwEAAtmgxgAB',
            'AgADMAEAAtmgxgAB',
            'AgADMQEAAtmgxgAB',
            'AgADMgEAAtmgxgAB',
            'AgADMwEAAtmgxgAB',
            'AgADNAEAAtmgxgAB',
            'AgADNQEAAtmgxgAB',
            'AgADNgEAAtmgxgAB',
            'AgADNwEAAtmgxgAB',
            'AgADOAEAAtmgxgAB',
            'AgADOQEAAtmgxgAB',
            'AgADOgEAAtmgxgAB',
            'AgADOwEAAtmgxgAB',
            'AgADPAEAAtmgxgAB',
            'AgADPQEAAtmgxgAB',
            'AgADPgEAAtmgxgAB',
            'AgADPwEAAtmgxgAB',
            'AgADQAEAAtmgxgAB',
            'AgADQQEAAtmgxgAB',
            'AgADQgEAAtmgxgAB',
    'AgADQwEAAtmgxgAB']


    cardtypes=("hearts","clubs","diamonds","spades")
    numbers=range(1,14)

    def __init__(self,id,id_unique,types,number):
          self.id=id
          self.id_unique=id_unique
          self.type=types
          self.number=number
