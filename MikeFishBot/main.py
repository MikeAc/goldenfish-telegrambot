"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


"""





from telegram.ext import (Updater,CommandHandler,MessageHandler,InlineQueryHandler,Filters,ChosenInlineResultHandler,CallbackContext,CallbackQueryHandler)
from telegram import (Message,TelegramObject,InlineQuery,InlineQueryResultCachedSticker, CallbackQuery,Update,InputTextMessageContent,ParseMode,ChosenInlineResult)
from telegram import (InlineQueryResultArticle,InlineKeyboardButton,InlineKeyboardMarkup)
from FishGame import FishGame,FishPlayer
import traceback
from FishGame import Cards
from uuid import uuid4
import logging,copy
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)
logger = logging.getLogger(__name__)

emoji_cry='\U0001F62D'
emoji_smile='\U0001F603'
emoji_tongue='\U0001F61C'
emoji_unamused='\U0001F612'
emoji_surprised='\U0001F631'
class TelegramBot(Updater):

    static_counter=7000
    
    def __init__(self,*args,**kwargs):
         super().__init__("telegram bot KEY",use_context=True)
         dp=self.dispatcher
         dp.add_handler(CommandHandler("nuevo",self.newCommand))
         dp.add_handler(CommandHandler("empezar",self.startCommand))
         dp.add_handler(CommandHandler("unirse",self.joinCommand))
         dp.add_handler(CommandHandler("salir",self.exitCommand))
         dp.add_handler(CommandHandler("terminar",self.killCommand))
         dp.add_handler(CommandHandler("saltar",self.passCommand))
         dp.add_handler(CommandHandler("sacar",self.kickCommand))
         dp.add_handler(CommandHandler("ayuda",self.helpCommand))
         dp.add_handler(CommandHandler("sources",self.sourcesCommand))

         dp.add_handler(InlineQueryHandler(self.inlineQuery))
         dp.add_handler(CallbackQueryHandler(self.buttons))
         dp.add_handler(MessageHandler(Filters.sticker,self.ansSticker))
         dp.add_handler(MessageHandler(Filters.text,self.ansmessage))
         self.start_polling()
         self.idle()
    
    def ansmessage(self,update:Update,context:CallbackContext):
        player_id=update.message.from_user.id
        message=update.message.text
        logger.info(message)
        try:
            game,player=FishGame.searchPlayerbyId(player_id)
            if game.started and player.id==game.currPlayer.id:
               playerpref= message.split(" eligio a ")  
               player2=game.findPlayerbyPrefix(playerpref[1])
               player.cardtoAsk['player']=player2
               total=player.exchangeCards()
               poker=0
               if total > 0:
                   context.bot.send_message(game.romid,"{} le ha dado a {} {} carta(s)!!!".format(player2.prefix,player.prefix,total)+emoji_surprised)
                   poker=player.makePokers()
                   self.poker(game.romid,player.prefix,poker)
                   if player.status!=FishPlayer.PLAYER_OUTOFCARDS:
                       self.printinlineKeyboard(game.romid,context,"{} Vuelve a pedir".format(player.prefix))
                   else:
                       context.bot.send_message(game.romid,"{} Ya no tiene mas cartas".format(player.prefix))
                       self.nextMessage(game,context)
               else:
                   context.bot.send_message (game.romid,"{} ups jugador {} no tenia esa carta, ahora saca una carta".format(player.prefix,player2.prefix))
                   drawcard=player.draw()
                   if drawcard is not  None and  player.status==FishPlayer.PLAYER_ASKS_CARD:
                       context.bot.send_message(game.romid,"OMG {} Ha sacado la  carta que pidio a {}.\n".format(player.prefix,player2.prefix)+emoji_surprised)
                       poker=player.makePokers()
                       self.poker(game.romid,player.prefix,poker)
                       if player.status==FishPlayer.PLAYER_OUTOFCARDS:
                           self.nextMessage(game,context)
                       else:
                            self.printinlineKeyboard(game.romid,context,"{} Puedes volver a pedir: .\n".format(player.prefix))


                   else:
                       poker=player.makePokers()
                       self.poker(game.romid,player.prefix,poker)
                       self.nextMessage(game,context)
                                        

        except Exception as e :
            traceback.print_exc()
        
    def newCommand(self,update,context):
        
        group_type= update.message.chat.type
        logger.info("Chat type '%s'",group_type)
        if "group" in group_type:
            chat_id=copy.deepcopy(update.message.chat.id)
            logger.info("Chat id '%d'", chat_id)
            player=FishGame.searchPlayerbyId(update.message.from_user.id)
            if player is not None:
                update.message.reply_text("Ya estas unido en la partida "+emoji_tongue)
                return

            game=FishGame(chat_id)
            if FishGame.createNewgame(game):
                update.message.reply_text("Nuevo juego creado")
            else:
                update.message.reply_text("Ya hay un juego abierto  "+emoji_cry)
        else:
            update.message.reply_text("Solo puede crearse el juego en un grupo "+emoji_cry)

    def joinCommand(self,update,context):
        player_id=update.message.from_user.id
        player_name=update.message.from_user.username
        player_prefix=update.message.from_user.name
        chat_id=update.message.chat.id
        player=FishPlayer(player_id,player_name,player_prefix)
        joinStatus=FishGame.joinPlayer(player,chat_id)
        if joinStatus == FishGame.PLAYER_JOINED:
            update.message.reply_text("Te has unido "+emoji_smile)
        elif joinStatus ==FishGame.PLAYER_ALREADY_JOINED:
            update.message.reply_text("Ya estas  en la partida "+emoji_tongue)
        elif joinStatus ==FishGame.NOGAMECREATED:
            update.message.reply_text("No hay ningun juego al  cual unirse  "+emoji_cry)
        logger.info("Player id %d username %s", player_id,player_name)

    def exitCommand(self,update:Update,context):
        
        player_id=update.message.from_user.id
        player_name=update.message.from_user.name
        chat_id=update.message.chat.id
        removeStatus=FishGame.removePlayer(player_id,chat_id)
        if removeStatus==FishGame.PLAYER_REMOVED:
            update.message.reply_text("Ok, "+player_name+" has salido de la partida")
        elif removeStatus ==FishGame.PLAYER_NOTFOUND:
            update.message.reply_text("No estas dentro  de ninguna partida "+emoji_unamused)
        else:
                update.message.reply_text("Ok, el juego ha sido eliminado por no haber nadie")

    def killCommand(self,update:Update,context):
        player_id=update.message.from_user.id
        chat_id=update.message.chat.id
        killStatus=FishGame.killGame(player_id,chat_id)
        if killStatus == FishGame.PLAYER_NOTFOUND or killStatus== FishGame.NOGAMECREATED:
            update.message.reply_text("No estas dentro  de ninguna partida "+emoji_unamused)
        elif killStatus == FishGame.GAME_DELETED:
            context.bot.send_message(chat_id,"El juego ha sido terminado")

    def startCommand(self,update:Update,context):

        player_id=update.message.from_user.id
        chat_id=update.message.chat.id
        player=FishGame.start(player_id,chat_id)
        if player is None:
            update.message.reply_text("No estas dentro  de ninguna partida "+emoji_unamused)
        elif isinstance(player,int)  :
            if player!= FishGame.NEEDS_PLAYERS:
                update.message.reply_text("Ya  estas en una partida "+emoji_unamused)
            else:
                update.message.reply_text("Falta al menos un jugador "+emoji_unamused)
        else:

            self.printinlineKeyboard(chat_id,context,"Primer jugador: "+player.prefix +" "+emoji_smile)
            poker=player.makePokers()
            self.poker(chat_id,player_id,poker)


    def inlineQuery(self,update:Update,context):
        player_id=update.inline_query.from_user.id
        game,player=FishGame.searchPlayerbyId(player_id)
        results = []
        try:
            if game.started:
                if player.status == FishPlayer.PLAYER_ASKS_NOTHING or player.status == FishPlayer.PLAYER_ASKS_CARD:
                    results=self.showUserCards(game,player)
                elif player.status ==FishPlayer.PLAYER_ASKS_PLAYER:
                    results=self.showinGamePlayers(player,game)
                    
            update.inline_query.answer(results=results,cache_time=1,is_personal=True)
        except Exception as e:
            print (e.args)

    def showUserCards(self,game,player):
        results=[]
        if not player.cards:
            results.append(InlineQueryResultCachedSticker(
                id="7777:{}:{}".format("ask","0"),
                sticker_file_id="CAACAgQAAxkBAAIET16Yw45umMcTIU_JqF_mrFaooHJ5AALEAgACX1eZAAGi2Qy93IIQwhgE",
                input_message_content=InputTextMessageContent( message_text=game.formatPlayerlist())))
            return results

        for cards in player.cards:
            print("type  "+cards.type+" num "+str(cards.number))
            results.append(
                    InlineQueryResultCachedSticker(
                        id="7777:{}:{}".format(cards.type,cards.number),
                        sticker_file_id=cards.id,
                        input_message_content=None if game.currPlayer.id==player.id else InputTextMessageContent(
                            message_text=game.formatPlayerlist())))
        if game.currPlayer.id==player.id:
            results.append(InlineQueryResultCachedSticker(
                    id="7777:{}:{}".format("ask","0"),
                    sticker_file_id="CAACAgQAAxkBAAIET16Yw45umMcTIU_JqF_mrFaooHJ5AALEAgACX1eZAAGi2Qy93IIQwhgE",
                    input_message_content=InputTextMessageContent(
                        message_text=game.formatPlayerlist())))

        return results
    
    def showinGamePlayers(self,currplayer,game: FishGame):
        results=[]
        for  player in game.playerlist:
            if player.id==currplayer.id:
                continue
            results.append(
                    InlineQueryResultArticle(id=uuid4(),
                        title=player.name,
                        input_message_content=InputTextMessageContent("{} eligio a {}".format(currplayer.prefix,player.prefix))))
        return results


    def ansSticker(self,update:Update,context):
        chosen_inline_playerid=update.message.from_user.id
        card_id=update.message.sticker.file_unique_id
        try:
            game,player=FishGame.searchPlayerbyId(chosen_inline_playerid)
            if game.started and player.id==game.currPlayer.id:
                print("entro2")
                print(player.status)
                if player.status==FishPlayer.PLAYER_ASKS_CARD:
                    print("entro3")
                    card=player.findCard(card_id)
                    player.cardtoAsk['card']=card
                    player.status=FishPlayer.PLAYER_ASKS_PLAYER
                    self.printinlineKeyboard(game.romid,context,"{}  dime, a quien pediras esa carta? ".format( player.prefix))
        
        except Exception as e:
            traceback.print_exc()
            logger.warning(e.args)
    
    def printinlineKeyboard(self,romid,context,txt):
        keyboard=[[InlineKeyboardButton("Elije!!",switch_inline_query_current_chat="")]]
        markup=InlineKeyboardMarkup(keyboard)
        context.bot.send_message(romid,txt,reply_markup=markup)
    
    def buttons(self,update:Updater,context):
        query = update.callback_query
        query.answer()

    def poker(self, romid,username,poker):
        if poker>0:
            self.bot.send_message(romid,"{} ha sacado un poker de {}".format(username,poker))

    def nextMessage(self,game,context):
        if game.next() == FishGame.OK:
            player=game.currPlayer
            self.printinlineKeyboard(game.romid,context,"Turno de: {}".format(player.prefix))
        else:
            context.bot.send_message(game.romid,"GAMEOVER")
            context.bot.send_message(game.romid,"Resultados:\n"+game.formatPlayerlist())
            winner=game.gameOver()
            context.bot.send_message(game.romid,"{} ha ganado".format(winner.prefix))
            FishGame.killGame(player.id,game.romid)
    
    def passCommand(self,update:Update,context):
        player_id=update.message.from_user.id
        game,player=FishGame.searchPlayerbyId(player_id)
        try:
            self.nextMessage(game,context)
        except Exception as e:
            logger.warning(e.args)

            pass


    
    def kickCommand(self,update:Update,context):
        
        text=update.message.text
        """player_name=update.message.from_user.name
        chat_id=update.message.chat.id"""

        args=text.split(" ")
        if len(args)>1:
            chat_id=update.message.chat.id
            game,player=FishGame.searchPlayerbyPref(args[1])
            player_pref=player.prefix
            removeStatus=FishGame.removePlayer(player.id,chat_id)
            if game.currPlayer.id==player.id:
                    self.nextMessage(game,context)
            if removeStatus==FishGame.PLAYER_REMOVED:
                update.message.reply_text("Ok, "+player_pref+" has salido de la partida")
            elif removeStatus ==FishGame.PLAYER_NOTFOUND:
                update.message.reply_text("No estas dentro  de ninguna partida "+emoji_unamused)
            else:
                update.message.reply_text("Ok, el juego ha sido eliminado por no haber nadie")




    def helpCommand(self,update:Update,context):
        ins=open("inst.txt","r")
        txt=""
        for line in ins:
            txt+=line
        update.message.reply_text(txt)
        ins.close()


    def sourcesCommand(self,update:Updater,context):
        update.message.reply_text("https://gitlab.com/MikeAc/goldenfish-telegrambot")










        
if __name__=='__main__':
    bot=TelegramBot()





